import os
from multiprocessing import Process

from pyftpdlib import servers
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler

from chia_plotting_manager.agent.config import FTP_USER, FTP_PASSWORD, FTP_HOST, FTP_PORT, FTP_BASE_DIRECTORY


def start_ftp_server(directory=FTP_BASE_DIRECTORY):
    ftp_server = Process(target=FTPServer(directory).start)
    ftp_server.start()
    return ftp_server


class FTPServer(object):
    def __init__(self, base_dir: str, ftp_host=FTP_HOST, ftp_port=FTP_PORT):
        self.ftp_host = ftp_host
        self.ftp_port = ftp_port
        self.base_dir = base_dir

    def start(self):
        authorizer = DummyAuthorizer()
        authorizer.add_user(FTP_USER, FTP_PASSWORD, os.path.join(self.base_dir, ''), perm="elradfmwMT")

        handler = FTPHandler
        handler.authorizer = authorizer

        server = servers.FTPServer((self.ftp_host, self.ftp_port), handler)
        server.serve_forever()
