import logging
import os
import random
import tempfile

logger = logging.getLogger(__name__)

SOCKET_BUFFER_READ_BYTES = 1024 * 2
PLOT_SPACE_REQUIRED = 104
CHIA_MANAGER_MASTER_HOST = os.environ.get('CHIA_MANAGER_MASTER_HOST', "0.0.0.0")
CHIA_MANAGER_MASTER_PORT = int(os.environ.get('CHIA_MANAGER_MASTER_PORT', 9998))
FTP_HOST = os.environ.get('CHIA_MANAGER_FTP_HOST', "0.0.0.0")
FTP_PORT = int(os.environ.get('CHIA_MANAGER_FTP_PORT', random.randint(9999, 10100)))
FTP_USER = os.environ.get('CHIA_MANAGER_FTP_USER', "username")
FTP_PASSWORD = os.environ.get('CHIA_MANAGER_FTP_PASSWORD', "password")
FTP_BASE_DIRECTORY = os.environ.get('CHIA_FTP_BASE_DIRECTORY', tempfile.gettempdir())
CHECKSUM_TRANSFER_VERIFICATION = False
TEST_MODE = bool(os.environ.get('CHIA_MANAGER_TEST_MODE', False))
