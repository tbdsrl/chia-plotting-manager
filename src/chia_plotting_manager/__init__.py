import sys
import threading
import time
import subprocess
import logging

from chia_plotting_manager.metrics import collector
from chia_plotting_manager.utils import collect_path_stats, print_path_stats

logging.basicConfig(stream=sys.stdout,
                    format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

def run(chia_plot_size, chia_plot_size_gb, chia_plots_destinations,
        chia_plots_temps, chia_executable, farmer_public_key, pool_public_key,
        pool_contract_address, fake=False):
    logger.info('Starting manager:')
    stats = collect_path_stats(chia_plots_destinations)
    print_path_stats(stats)
    threads = []
    for chia_plots_temp in chia_plots_temps:
        run_params = [chia_plot_size, chia_plot_size_gb, chia_plots_destinations,
                      chia_plots_temp, chia_executable, farmer_public_key,
                      pool_public_key, pool_contract_address, fake]
        worker = threading.Thread(target=run_manager, args=run_params)
        worker.start()
        threads.append(worker)

    [ t.join() for t in threads]


def run_manager(chia_plot_size, chia_plot_size_gb, chia_plots_destinations,
                chia_plots_temp, chia_executable, farmer_public_key, pool_public_key,
                pool_contract_address, fake):
    while(True):
        stats = collect_path_stats(chia_plots_destinations)
        for chia_plots_destination in chia_plots_destinations:
            logger.info(f"Checking available space in final destination directory {chia_plots_destination}")
            free_space_gb = stats[chia_plots_destination]['hdd.free']
            if(free_space_gb > chia_plot_size_gb):
                cmd = [
                    chia_executable, "plots", "create",
                    "--num", "1", "--size", str(chia_plot_size), "--override-k",
                    "--final_dir", chia_plots_destination,
                    "--tmp_dir", chia_plots_temp,
                    "--farmer_public_key", farmer_public_key,
                ]
                if pool_public_key:
                    cmd += ["--pool_public_key", pool_public_key]
                if pool_contract_address:
                    cmd += ["--pool_contract_address", pool_contract_address]

                logger.info(f"Start to generate new plot with command '{' '.join(cmd)}'")
                start = time.time()
                if fake:
                    time.sleep(10)
                else:
                    subprocess.run(cmd)
                duration = time.time() - start
                logger.info(f"Plot generation completed {duration}s ({(duration) / 60 / 60 }h)")
                collector.gauge("plot", "total_plotting_time", "The total time spent to create the plot", duration)
                collector.gauge("plot", "total_available_plots", "The number of available plots in the directory", stats[chia_plots_destination]['plots.number'])
            else:
                logger.info(f"No space left to create new plots, only {free_space_gb}GB available on '{chia_plots_destination}'")

        print_path_stats(stats)
        time.sleep(10)
