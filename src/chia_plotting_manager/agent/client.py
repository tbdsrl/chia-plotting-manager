import json
import os
import shutil
import socket
import glob
import itertools
import sys
import time
import ftplib
import logging
import traceback

from chia_plotting_manager.agent.balancer import start_ftp_server
from chia_plotting_manager.agent.config import FTP_HOST, FTP_PORT, FTP_USER, FTP_PASSWORD, CHIA_MANAGER_MASTER_HOST, \
    CHIA_MANAGER_MASTER_PORT, FTP_BASE_DIRECTORY, SOCKET_BUFFER_READ_BYTES
from chia_plotting_manager.utils import file_md5, collect_path_stats, json_send, json_receive, STATUS_NO_PLOTS, STATUS_OK

logger = logging.getLogger(__name__)

def send_file(host, port, path, filename):
    session = ftplib.FTP()
    session.connect(host, port)
    session.login(FTP_USER, FTP_PASSWORD)
    with open(path, 'rb') as file:
        session.storbinary(f'STOR {filename}', file)
    session.quit()


def get_file(path: str, filename: str):
    session = ftplib.FTP()
    session.connect(FTP_HOST, FTP_PORT)
    session.login(FTP_USER, FTP_PASSWORD)
    with open(os.path.join(path, filename), 'wb') as file:
        session.retrbinary('RETR %s' % filename, file.write)


def run_client(chia_plots_destinations, is_silo=False, failfast=False):
    failures = 0
    while True:
        try:
            _run_client(chia_plots_destinations, is_silo)
        except ConnectionRefusedError:
            if failfast: raise
            sleep_time = min([1 * failures, 60])
            logger.error(f"Connection refused (retry in {sleep_time}s)")
            logger.error(traceback.format_exc())
            time.sleep(sleep_time)
            failures += 1
        except Exception as exc:
            if failfast: raise
            sleep_time = min([1 * failures, 60])
            logger.error(f"Client exception {exc} (retry in {sleep_time}s)")
            logger.error(traceback.format_exc())
            time.sleep(sleep_time)
            failures += 1



def get_path_with_most_space(path_stats):
    for path, _ in sorted(path_stats.items(), key=lambda kv: kv[1]['hdd.free'], reverse=True):
        return path


def _run_client(chia_plots_destinations, is_silo):
    # Create a socket (SOCK_STREAM means a TCP socket)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((CHIA_MANAGER_MASTER_HOST, CHIA_MANAGER_MASTER_PORT))

        def send(data):
            return json_send(sock, data)

        def receive():
            return json_receive(sock)

        send({
            'role': 'silo' if is_silo else 'plotter',
            'ftp_port': FTP_PORT,
            'decommissioning': False,
         })
        assert receive()['status'] == 'ok'
        logger.info('connected to master server for reporting')
        while True:
            data = receive()
            logger.debug(data)
            path_stats = collect_path_stats(chia_plots_destinations)

            if data['action'] == 'status':
                number_of_plots = sum(s['plots.number'] for s in path_stats.values())
                status = {
                    'paths': path_stats,
                    'plots': number_of_plots,
                }
                logger.debug(status)
                send(status)

            elif data['action'] == 'ship':
                host, port = data['to']
                # Send one of the files to the master ftp directory
                for plot in itertools.chain(*[glob.glob(os.path.join(d, '*.plot')) for d in chia_plots_destinations]):
                    logger.info(f"Preparing to ship {plot} to silo {host}:{port}")
                    checksum = file_md5(plot)
                    logger.info(f'Ship plot to the silo {plot} checksum {checksum}')
                    filename = os.path.basename(plot)
                    send_file(host, port, plot, filename)
                    send({'status': 'ok', 'checksum': checksum, 'filename': filename, 'size': os.path.getsize(plot)})
                    logger.info(f'Plot shipped {filename} checksum: {checksum}')
                    confirm = receive()
                    assert confirm['status'] == 'ok'
                    logger.info(f'Shipped plot have been confimed as correct')
                    os.remove(plot)
                    logger.info(f'Remove local plot {plot}')
                    break
                else:
                    send({'status': STATUS_NO_PLOTS})

            elif data['action'] == 'prepare-to-stock':
                destination_path = get_path_with_most_space(path_stats)
                ftp_server = start_ftp_server(destination_path)
                try:
                    time.sleep(1)
                    send({'status': 'ready_to_receive'})
                    action = receive()
                    if action['action'] == 'stock':
                        filename = action['filename']
                        checksum = action['checksum']
                        size = action['size']
                        destination = os.path.join(destination_path, filename)
                        # assert receive()['status'] == STATUS_OK
                        assert checksum == file_md5(destination)
                        assert size == os.path.getsize(destination)
                        logger.info(f"Confirm file stockage {destination} checksum: {checksum}")
                        send({'status': 'ok', 'checksum': checksum})
                    else:
                        logger.info('No stock required')
                finally:
                    try: ftp_server.terminate()
                    except: pass


