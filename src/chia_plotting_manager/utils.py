import glob
import hashlib
import json
import logging
import os
from struct import pack, unpack

import psutil
from tabulate import tabulate

from chia_plotting_manager.agent.config import CHECKSUM_TRANSFER_VERIFICATION, SOCKET_BUFFER_READ_BYTES

logger = logging.getLogger(__name__)

STATUS_NO_PLOTS = 'no-plots'
STATUS_OK = 'ok'

def file_md5(path):
    if not CHECKSUM_TRANSFER_VERIFICATION:
        return "FAKE-HASH - enable CHECKSUM_TRANSFER_VERIFICATION"

    hash_md5 = hashlib.md5()
    with open(path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def collect_path_stats(paths):
    stats = {}
    for path in paths:
        hdd = psutil.disk_usage(path)
        free_space_gb = hdd.free / (2 ** 30)
        plots = glob.glob(os.path.join(path, '*.plot'))
        decommission = bool(glob.glob(os.path.join(path, 'chia-plotting-manager-decommission')))
        stats[path] = {
            'hdd.free': free_space_gb,
            'hdd.decommission': decommission,
            'plots.number': len(plots),
        }
    return stats


def print_path_stats(stats):
    logger.info(tabulate([
        [k, v['hdd.free'], v["plots.number"]] for k, v in stats.items()
        ], headers=['Paths', "Available space", "Available plots"]))


def json_send(sock, data):
    data = bytes(json.dumps(data), 'ascii')
    length = pack('>Q', len(data))
    sock.sendall(length)
    sock.sendall(data)
    # wait for ack
    sock.recv(1)


def json_receive(sock):
    # raw_rata = ""
    # while not raw_rata:
    raw_rata = sock.recv(8)
    logger.debug('raw_data %s', raw_rata)
    (length,) = unpack('>Q', raw_rata)
    data = b''
    while len(data) < length:
        # doing it in batches is generally better than trying
        # to do it all in one go, so I believe.
        to_read = length - len(data)
        data += sock.recv(
            SOCKET_BUFFER_READ_BYTES if to_read > SOCKET_BUFFER_READ_BYTES else to_read)

    assert len(b'\00') == 1
    sock.sendall(b'\00')

    try:
        logger.debug('raw_json %s', data)
        return json.loads(data)
    except json.decoder.JSONDecodeError:
        logger.error(f'Unparsable data "{data}"')
        raise
