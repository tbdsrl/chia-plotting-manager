import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="chia_plotting_manager",
    version="0.0.10",
    author="Saverio Mucci",
    author_email="save@instal.com",
    description="A simple manager to create plots untill space is available on disk",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/tbdsrl/chia-plotting-manager/src",
    project_urls={
        "Bug Tracker": "https://bitbucket.org/tbdsrl/chia-plotting-manager/src",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    scripts=[
        'bin/chia-plotting-manager',
        'bin/chia-plotting-manager-master',
    ],
    packages=setuptools.find_packages(where="src"),
    install_requires=[
        'psutil',
        'tabulate',
        'pyftpdlib',
    ],
    python_requires=">=3.6",
)
