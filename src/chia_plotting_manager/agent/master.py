import json
import logging
import socketserver
import socket
import threading
import time
import traceback

from chia_plotting_manager.agent.config import CHIA_MANAGER_MASTER_HOST, \
    CHIA_MANAGER_MASTER_PORT, PLOT_SPACE_REQUIRED, SOCKET_BUFFER_READ_BYTES, TEST_MODE
from chia_plotting_manager.utils import json_receive, json_send, STATUS_NO_PLOTS, STATUS_OK

logger = logging.getLogger(__name__)
relocation_lock = threading.Lock()

class ConnectionsStorage(object):
    def __init__(self):
        self.connections = []

    def add(self, connection):
        self.connections.append(connection)

    def remove(self, connection):
        self.connections.remove(connection)

    def get_silos(self):
        return [c for c in self.connections if c.is_silo]

    def get_harvesters(self):
        return [c for c in self.connections if not c.is_silo]

    def get_plotters(self):
        return [c for c in self.connections if not c.is_silo]


client_storage = ConnectionsStorage()


class ClientHandler(object):
    def __init__(self, tcp_hanlder: socketserver.BaseRequestHandler):
        self.tcp_handler = tcp_hanlder

        self.client_presentation = self.json_receive()
        self.json_send({'status': 'ok'})
        self.is_silo = self.client_presentation['role'] == 'silo'
        self.ftp_port = self.client_presentation['ftp_port']
        logger.info(f"Connected new client {self.client_presentation}")
        # example: {'paths': {'/Users/savemu/Desktop/chia_plotter_01': {'hdd.free': 66.4911880493164, 'plots.number': 0}, '/Users/savemu/Desktop/chia_plotter_02': {'hdd.free': 66.4911880493164, 'plots.number': 0}}, 'plots': 0}
        self.status = None

    def __str__(self):
        return f"ClientHandler {self.tcp_handler.client_address}"

    def json_send(self, data):
        return json_send(self.tcp_handler.request, data)

    def json_receive(self):
        return json_receive(self.tcp_handler.request)

    def get_is_decommissioning(self):
        return any(p.get('hdd.decommission') for p in self.status['paths'].values())

    def get_free_space(self):
        if self.get_is_decommissioning():
            return 0
        return max(p['hdd.free'] for p in self.status['paths'].values())

    def get_number_of_plots(self):
        return sum(p['plots.number'] for p in self.status['paths'].values())

    def get_address(self):
        return self.tcp_handler.client_address[0]

    def run_forever(self):
        while True:
            with relocation_lock:
                self.collect_status()
                self.evaluate_stocking_to_silo(client_storage.get_plotters())
                self.evaluate_stocking_to_silo(client_storage.get_silos())

            time.sleep(10)

    def evaluate_stocking_to_silo(self, source_nodes):
        logger.info(f'Evaluate stocking of plots on silo')
        for source in source_nodes:
            # check if all the plotting machines have space to plot a new file
            if source.get_free_space() < (PLOT_SPACE_REQUIRED * 2) and source.get_number_of_plots() > 0 or TEST_MODE:
                # In case the space is finished we move a plot in a silo with available space
                sorted_silos = sorted(client_storage.get_silos(), key=lambda s: s.get_free_space())
                for silo in sorted_silos:
                    if silo.get_free_space() > PLOT_SPACE_REQUIRED or TEST_MODE:
                        logger.info(f'Found silo where to store the plot {silo} available space {silo.get_free_space()}GB')
                        # Prepare silo to receive the file
                        silo.json_send({'action': 'prepare-to-stock'})
                        assert silo.json_receive()['status'] == 'ready_to_receive'
                        # Notiy the source that can ship the file
                        source.json_send({'action': 'ship', 'to': (silo.get_address(), silo.ftp_port)})
                        status = source.json_receive()
                        if status['status'] == STATUS_OK:
                            logger.info(f'Plot shipped to silo, wait silo for relocation {silo}, {status}')
                            filename = status['filename']
                            checksum = status['checksum']
                            size = status['size']
                            silo.json_send({'action': 'stock', 'filename': filename, 'checksum': checksum, 'size': size})
                            assert silo.json_receive()['status'] == 'ok'
                            logger.info(f'Silo confirmed {filename} storage')
                            source.json_send({'status': 'ok'})
                            logger.info(f'Moved plot form {source} to {silo} {checksum}')
                            return
                        elif status['status'] == STATUS_NO_PLOTS:
                            logger.info(f'No plot available for reloacation on {silo}, {status}')
                            silo.json_send({'action': 'abort'})
                            return


                logger.info(f'No silo available to store the plot')

    def collect_status(self):
        logger.debug(f'Fetch informations from the client {self}')
        self.json_send({'action': 'status'})
        self.status = self.json_receive()


def run_master():
    class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):

        def handle(self):
            try:
                client_handler = ClientHandler(self)
                client_storage.add(client_handler)
                client_handler.run_forever()
            except:
                logger.info(f'Disconnected client {client_handler}')
                client_storage.remove(client_handler)
                logger.error(traceback.format_exc())

    class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
        allow_reuse_address = True

    with ThreadedTCPServer((CHIA_MANAGER_MASTER_HOST, CHIA_MANAGER_MASTER_PORT), ThreadedTCPRequestHandler) as server:
        try:
            logger.info(f'Started master server {CHIA_MANAGER_MASTER_HOST}:{CHIA_MANAGER_MASTER_PORT} ')
            server.serve_forever()
        except KeyboardInterrupt:
            server.shutdown(socket.SHUT_RDWR)
            server.close()


