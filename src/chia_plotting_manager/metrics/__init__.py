import os
import logging
logger = logging.getLogger(__name__)

collectors = []
prometheus_available = False

try:
	from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
	prometheus_available = True
except ImportError:
	logger.info('No prometheus_client available no metrics will be sent to prometheus')


class PrometheusCollector(object):
	def __init__(self):
		self.push_to_gateway = os.environ.get('CHIA_PLOTTING_MANGER_PROMETHEUS_PUSH_GATEWAY') or 'localhost:9091'
		self.registry = CollectorRegistry()

	def gauge(self, job: str, metric: str, description: str, value: int):
		Gauge(metric, description, registry=self.registry).set(value)
		push_to_gateway(self.push_to_gateway, job=job, registry=self.registry)


if prometheus_available:
	collectors.append(PrometheusCollector())

class Collector(object):
	def gauge(self, job: str, metric: str, description: str, value: int):
		for collector in collectors:
			collector.gauge(job, metric, description, value)

collector = Collector()
